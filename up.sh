#!/usr/bin/env bash

build() {
	V1=$1
	V2=$2

	shift
	shift

	rm php$V1/box.sh

	cp toolbox/box.sh php$V1/box.sh

	docker build php$V1 --tag devenv:$V2 $*
}

build 56 5.6
build 70 7.0
build 71 7.1 --tag devenv:7 --tag devenv:latest
build 72 7.2

